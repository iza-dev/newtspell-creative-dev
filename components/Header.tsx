import React from "react";
import AppLink from "./AppLink";
import Revers from "./icons/Revers";
import styles from "../styles/_components/header.module.scss";

interface HeaderProps {}

const Header: React.FC<HeaderProps> = () => {
  return (
    <header className={styles.wapper_header}>
      <AppLink href="/" label="NS" />
      <nav>
        <ul>
          <li>
            <AppLink href="/gallery" label="Gallery" />
          </li>
          <li>
            <AppLink href="/labs" label="Labs" />
          </li>
          <li>
            <AppLink href="/genesis" label="Genesis" />
          </li>
          <li>
            <AppLink href="/about" label="About" />
          </li>
          <li className={styles.icon3D}>
            <Revers />
            coming soon
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
