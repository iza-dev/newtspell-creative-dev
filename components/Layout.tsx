import React, { useRef, useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Social from "./Social";
import styles from "../styles/_layouts/main.module.scss";

interface LayoutProps {}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div>
      <Social />
      <div className={styles.main_wrapper}>
        <Header />
        <div className={styles.container}>
          <main>{children}</main>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
