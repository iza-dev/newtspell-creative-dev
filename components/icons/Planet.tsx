import React from "react";
import styles from "../../styles/_components/icons.module.scss";

interface PlanetProps {}

const Planet: React.FC<PlanetProps> = () => {
  return (
    <div className={styles.wrapper_earth}>
      <div className={styles.earth}></div>
      <div className={styles.belt}>
        <span className={styles._1}>W</span>
        <span className={styles._2}>O</span>
        <span className={styles._3}>R</span>
        <span className={styles._4}>K</span>
        <span className={styles._5}>-</span>
        <span className={styles._6}>I</span>
        <span className={styles._7}>N</span>
        <span className={styles._8}>-</span>
        <span className={styles._9}>P</span>
        <span className={styles._10}>R</span>
        <span className={styles._11}>O</span>
        <span className={styles._12}>G</span>
        <span className={styles._13}>R</span>
        <span className={styles._14}>E</span>
        <span className={styles._15}>S</span>
        <span className={styles._16}>S</span>
        <span className={styles._17}>-</span>
      </div>
    </div>
  );
};
export default Planet;
