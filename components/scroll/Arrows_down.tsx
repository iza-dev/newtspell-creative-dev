import React from 'react'

interface ArrowsProps{}

const Arrows: React.FC<ArrowsProps> = () => {
    return (
        <svg width="34" height="50" viewBox="0 0 34 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_f_123:568)">
                <path d="M10.4975 28.1692L17 34.6575L23.5025 28.1692L25.5 30.1667L17 38.6667L8.5 30.1667L10.4975 28.1692Z" fill="#FF0080"/>
            </g>
            <g filter="url(#filter1_f_123:568)">
                <path d="M10.4975 20.1692L17 26.6575L23.5025 20.1692L25.5 22.1667L17 30.6667L8.5 22.1667L10.4975 20.1692Z" fill="#FF0080"/>
            </g>
            <g filter="url(#filter2_f_123:568)">
                <path d="M10.4975 12.1692L17 18.6575L23.5025 12.1692L25.5 14.1667L17 22.6667L8.5 14.1667L10.4975 12.1692Z" fill="#FF0080"/>
            </g>
            <defs>
                <filter id="filter0_f_123:568" x="-4" y="12" width="42" height="42" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                    <feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur_123:568"/>
                </filter>
                <filter id="filter1_f_123:568" x="-4" y="4" width="42" height="42" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                    <feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur_123:568"/>
                </filter>
                <filter id="filter2_f_123:568" x="-4" y="-4" width="42" height="42" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                    <feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur_123:568"/>
                </filter>
            </defs>
        </svg>
    )
}

export default Arrows