import React from 'react'

interface LineProps{}

const Line: React.FC<LineProps> = () => {
    return (
        <svg width="35" height="74" viewBox="0 0 1 74" fill="none" xmlns="http://www.w3.org/2000/svg">
            <line x1="0.5" x2="0.5" y2="74" stroke="#7C7C7C"/>
        </svg>
    )
}

export default Line