import React from "react";
import styles from "../styles/_components/cardHolo.module.scss";
// import Galaxy from "../components/icons/Galaxy";
import Planet from "../components/icons/Planet";

interface CardHologramProps {}

const CardHologram: React.FC<CardHologramProps> = () => {
  return (
    <div className={styles.wrapper_card}>
      <div className={styles.content_card}>
        <div className={styles.title_card}>
          <div className={styles.content_title_card}>
            <div className={styles.title_card}>
              <h3>Project name</h3>
            </div>
            <p>#id 08/10</p>
          </div>
        </div>
        <div className={styles.link_mint}>
          <div className={styles.content_link_mint}>
            <p>Mint on</p>
            <div className={styles.button}>
              <p>//Foundation</p>
            </div>
          </div>
        </div>
        <div className={styles.wrapper_visual_project}>
          <div className={styles.content_visual_project}>
            <h4>Collection name</h4>
            <div className={styles.visual_project}>
              <Planet />
            </div>
            <h4>Serie name</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardHologram;
