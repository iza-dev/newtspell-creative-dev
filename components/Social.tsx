import React from 'react'
import Instragram from './icons/Instagram'
import Twitter from './icons/Twitter'
import styles from '../styles/_components/social.module.scss'

interface SocialProps{}

const Social: React.FC<SocialProps> = () => {
    return ( 
        <div className={styles.wrapper_social}>
            <a href="#">
                <Instragram />
            </a>
            <a href="#">
                <Twitter />
            </a>
        </div>
    )
}

export default Social