import React from 'react'
import styles from '../styles/_components/footer.module.scss'

interface FooterProps{}

const Footer: React.FC<FooterProps> = () => {
    return (
        <footer className={styles.wrapper_footer}>
            <p>contact@newtspell.tech</p>
        </footer>
    )
}

export default Footer
