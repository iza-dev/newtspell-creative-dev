import React from "react";
import styles from "../styles/_components/gallery.module.scss";

interface CardProps {}

const Card: React.FC<CardProps> = () => {
  return (
    <div className={styles.wrapper_card}>
      <div className={styles.content_card}>
        <div className={styles.title_card}>
          <div className={styles.content_title_card}>
            <h3>Project name</h3>
            <p>#id 08/10</p>
          </div>
        </div>
        <div className={styles.link_mint}>
          <div className={styles.content_link_mint}>
            <p>Mint on</p>
            <div className={styles.button}>
              <p>//Foundation</p>
            </div>
          </div>
        </div>
        <div className={styles.wrapper_visual_project}>
          <div className={styles.content_visual_project}>
            <h4>Collection name</h4>
            <div className={styles.visual_project}></div>
            <h4>Serie name</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
