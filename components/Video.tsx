import React from "react";

interface Bg_videoProps {}

const Bg_video: React.FC<Bg_videoProps> = () => {
  return (
    <video autoPlay muted loop preload="auto">
      <source src="./videos/galaxy_loop.mp4" type="video/mp4" />
      <source src="./videos/galaxy_loop.webm" type="video/webm" />
    </video>
  );
};

export default Bg_video;
