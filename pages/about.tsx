import Layout from "../components/Layout";

interface AboutProps {}

const About: React.FC<AboutProps> = () => {
  return (
    <Layout>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur, modi
        omnis voluptate atque veritatis maxime libero laboriosam exercitationem
        amet, hic, optio assumenda tempora voluptates impedit? Quasi temporibus
        nostrum perspiciatis, corrupti voluptas eveniet excepturi odio ab.
        Repellendus fugiat dolor rem accusamus possimus incidunt illo
        reprehenderit tempora veniam, quis tenetur, qui error, assumenda
        architecto ipsam ducimus et rerum modi facere aliquid quidem! Labore
        debitis porro aspernatur quis sint culpa facere quod officia tenetur
        ullam est, iste qui. Voluptas corrupti natus voluptatibus corporis.
      </p>
    </Layout>
  );
};

export default About;
