import Layout from "../components/Layout";
import styles from "../styles/_layouts/genesis.module.scss";

interface GenesisProps {}

const Genesis: React.FC<GenesisProps> = () => {
  return (
    <Layout>
      <p>Genesis</p>
    </Layout>
  );
};

export default Genesis;
