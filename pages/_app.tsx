import React, { useEffect } from "react";
import Head from "next/head";
import { useRouter } from 'next/router';
import type { AppProps } from 'next/app';
import Bg_video from '../components/Video';
import '../styles/_base/_reset.scss';
import '../styles/main.scss';

function MyApp({ Component, pageProps }: AppProps) {
  const url = useRouter().pathname;
  let background_dark;
  let video_bg: HTMLVideoElement | null;

  useEffect(() => {
    video_bg = document.querySelector("video")
    video_bg?.addEventListener("canplay", function(e) {
      this.play(); 
    }, false)
  }, []);

  if(url==="/genesis" || url==="/about"){
    background_dark = <div className="filter_dark"></div>;
  } else{
    background_dark = null;
  }

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <title>Newtspell • HOME</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
        />
        <meta
          name="description"
          content="Presentation of the art of Newtspell, an artist who offers the vision of another world where lust reigns. Abandonment and oblivion allows us to break down the masks and the borders that limit the mind and the body."
        />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_bold-webfont.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_extra_light-webfont.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_regular-webfont.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_bold-webfont.woff"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_extra_light-webfont.woff"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_regular-webfont.woff"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_bold-webfont.woff2"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_extra_light-webfont.woff2"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/FaunaPro/fauna_pro_-_regular-webfont.woff2"
          as="font"
          crossOrigin=""
        />

        <link
          rel="preload"
          href="/fonts/Halcyon/halcyon_regular-webfont.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Halcyon/halcyon_thin-webfont.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Halcyon/halcyon_regular-webfont.woff"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Halcyon/halcyon_thin-webfont.woff"
          as="font"
          crossOrigin=""
        />
      </Head>
      {background_dark}
      <Bg_video/>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp