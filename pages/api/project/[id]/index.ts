import { NextApiRequest, NextApiResponse } from "next";

export default function getProjectById(
  req: NextApiRequest,
  res: NextApiResponse
) {
  res.json({ id: req.query.id, msg: "get project by id" });
}
