import type { NextPage } from 'next'
import Layout from '../components/Layout'
import styles from '../styles/_layouts/home.module.scss'

const Home: NextPage = () => {
  return (
    <Layout >
      <section className={styles.wrapper_text}>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus beatae assumenda adipisci iste? Animi odio rem reiciendis quo repellat debitis ullam nihil, molestiae, quam voluptas illo quasi quos totam repellendus. Atque ipsa qui ullam doloribus alias temporibus sit necessitatibus aperiam!</p>
      </section>
    </Layout>
  )
}
 
export default Home
