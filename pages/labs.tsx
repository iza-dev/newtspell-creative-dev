import Layout from "../components/Layout";
import CardHologram from "../components/CardHologram";
import styles from "../styles/_components/cardHolo.module.scss";

interface LabsProps {}

const Labs: React.FC<LabsProps> = () => {
  return (
    <Layout>
      <div className={styles.wrapper_cards}>
        <CardHologram />
        <CardHologram />
        <CardHologram />
        <CardHologram />
      </div>
    </Layout>
  );
};

export default Labs;
