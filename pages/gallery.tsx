import React, { useRef, useEffect, useState } from "react";
import * as THREE from "three";
import Layout from "../components/Layout";
import styles from "../styles/_components/gallery.module.scss";

interface GalleryProps {}

//https://dev.to/vvo/how-to-solve-window-is-not-defined-errors-in-react-and-next-js-5f97

const Gallery: React.ForwardedRef<GalleryProps> = () => {
  const containerCanvas3D = useRef<HTMLDivElement>(null);
  const mountRef = useRef<HTMLDivElement>(null);
  const [scale, setScale] = React.useState({
    width: 250,
    height: 350,
  });

  if (typeof window !== "undefined") console.log(window.innerWidth);

  const resized = () => {
    console.log(containerCanvas3D?.current?.offsetWidth);
    if (containerCanvas3D.current)
      setScale({
        width: containerCanvas3D.current.offsetWidth,
        height: containerCanvas3D.current.offsetHeight,
      });
  };

  useEffect(() => {
    if (scale.width && scale.height) {
      var scene = new THREE.Scene();

      var camera = new THREE.PerspectiveCamera(
        75,
        scale.width / scale.height,
        0.1,
        1000
      );
      var renderer = new THREE.WebGLRenderer({ alpha: true });
      renderer.setSize(scale.width, scale.height);
      renderer.setPixelRatio(window.devicePixelRatio);

      if (mountRef.current) mountRef.current.appendChild(renderer.domElement);

      var geometry = new THREE.PlaneGeometry(9, 7.5);
      var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
      var plane = new THREE.Mesh(geometry, material);
      scene.add(plane);
      camera.position.z = 5;
      camera.aspect = scale.width / scale.height;
      camera.updateProjectionMatrix();

      var animate = function() {
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
      };

      animate();

      return () => {
        mountRef?.current?.removeChild(renderer.domElement);
      };
    }
  }, [scale]);

  useEffect(() => {
    if (mountRef.current) {
      const currentCanvas = mountRef.current;
      window.addEventListener("resize", resized);
      return () => window.removeEventListener("resize", resized);
    }
  });

  return (
    <Layout>
      <div id={styles.wrapper_list_saison}>
        <div id={styles.container_list}>
          <ul id={styles.list_saison}>
            <li>Saison spring/summer 2022</li>
            <li>Saison • automn/winter 2022</li>
            <li id={styles.points}>•••</li>
          </ul>
        </div>
        <div id={styles.text_presentation}>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing.</p>
          <div id="canvasForPreviewSaison" ref={containerCanvas3D}>
            <div id="viewport" ref={mountRef}></div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Gallery;
